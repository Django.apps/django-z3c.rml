from django.apps import AppConfig


class DjangoPdfConfig(AppConfig):
    name = 'django_pdf'
